package controller

import (
	"bytes"
	"io"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)
func TestAddBook(t *testing.T) {
	url := "http://localhost:8082/books"
	var jsonStr = []byte(`{"isgn":1234567, "title": "Karmaaa", "author": "Dooorji", "genre": "Scii-Ficc"}`)
	req, _ := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")
	
	client := &http.Client{}
	resp, err := client.Do(req)
	// cookie, err := resp.Cookie("my-cookie")
	
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
	
	body, _ := io.ReadAll(resp.Body)
	assert.Equal(t, http.StatusCreated, resp.StatusCode)
	expResp := `{"message": "added successfully"}`
	assert.JSONEq(t, expResp, string(body))
}

func TestGetBook(t *testing.T) {
	c := http.Client{}
	r, _ := c.Get("http://localhost:8082/books/12220048")
	
	body, _ := io.ReadAll(r.Body)
	assert.Equal(t, http.StatusOK, r.StatusCode)
	expResp := `{"isbn":12220048, "title": "Karma", "author": "Dorji", "genre": "Sci"}`
	assert.JSONEq(t, expResp, string(body))
}

func TestDeleteBook(t *testing.T) {
	url := "http://localhost:8082/books/12220048"
	req, _ := http.NewRequest("DELETE", url, nil)
	client := &http.Client{}
	resp, err := client.Do(req)
	
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
	body, _ := io.ReadAll(resp.Body)
	assert.Equal(t, http.StatusOK, resp.StatusCode)
	expResp := `{"status":"deleted successfully"}`
	assert.JSONEq(t, expResp, string(body))
}

func TestBookNotFound(t *testing.T) {
	assert := assert.New(t)
	c := http.Client{}
	r, _ := c.Get("http://localhost:8082/books/101")
	body, _ := io.ReadAll(r.Body)
	assert.Equal(http.StatusNotFound, r.StatusCode)
	expResp := `{"error":"book not found"}`
	assert.JSONEq(expResp,string(body))
}