package controller

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"myapp/model"
	"myapp/utils/httpResponse"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

func Enroll(w http.ResponseWriter, r *http.Request){
	fmt.Println("lingmkkjsd")
	var log model.Log 
	err := json.NewDecoder(r.Body).Decode(&log)
	if err != nil{
		fmt.Println("error in decoding the request")
		return 
	}
	gErr := log.LogData()
	if gErr != nil {
		fmt.Println("error in inserting the data",gErr)
	}
}
func ShowEnrolled(w http.ResponseWriter,r *http.Request) {
	id := mux.Vars(r)["stdid"]
	sid ,err:= strconv.Atoi(id)
	if err != nil{
		fmt.Println("error in converting")
	}
	title := mux.Vars(r)["title"]
	var log model.Log
	getErr := log.GetLoggedData(sid,title)
	if getErr != nil{
		switch getErr{
		case sql.ErrNoRows:
			fmt.Println("no data")
		default:
			fmt.Println("error in getting the database")
		}
		return 
	}
	httpResponse.ResponseWithJson(w,http.StatusOK,log)
	fmt.Println("success")
}
func GetAllEnrolled(w http.ResponseWriter, r *http.Request) {
	fmt.Println("kkkkkkkkkkkkkkk")
	enroll,getErr := model.GetAllEnrolledData();
	if getErr != nil{
		httpResponse.ResponseWithError(w,http.StatusNotFound,"data not found")
		return 
	}
	httpResponse.ResponseWithJson(w,http.StatusOK,enroll)
	fmt.Println("ksdlfksdf",enroll)
}

func DeleteLog(w http.ResponseWriter,r *http.Request){
	id := mux.Vars(r)["stdid"]
	sid ,err:= strconv.Atoi(id)
	if err != nil{
		fmt.Println("error in converting")
	}
	title := mux.Vars(r)["title"]
	dErr := model.DeleteLoggedData(sid,title)
	if dErr != nil{
		fmt.Println("not working",dErr)
	}
}