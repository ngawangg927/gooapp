package controller

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"myapp/model"
	"myapp/utils/httpResponse"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

func AddBook(w http.ResponseWriter, r *http.Request) {
var book model.Book
decoder := json.NewDecoder(r.Body)
err := decoder.Decode(&book)
if err != nil {
httpResponse.ResponseWithError(w, http.StatusBadRequest, "invalid json data")
return
}
fmt.Println(book)
dbErr := book.Create()
if dbErr != nil {
httpResponse.ResponseWithError(w, http.StatusBadRequest, dbErr.Error())
return
}
httpResponse.ResponseWithJson(w, http.StatusCreated, map[string]string{"message": "added successfully"})
}

func GetBook(w http.ResponseWriter, r *http.Request) {
bookID := mux.Vars(r)["isbn"]
id, idErr := strconv.ParseInt(bookID, 10, 64)
if idErr != nil {
httpResponse.ResponseWithError(w, http.StatusBadRequest, "invalid book isbn")
return
}
book := model.Book{ISBN: id}
fmt.Println(book)
getErr := book.Read()
if getErr != nil {
switch getErr {
case sql.ErrNoRows:
httpResponse.ResponseWithError(w, http.StatusNotFound, "book not found")
default:
httpResponse.ResponseWithError(w, http.StatusInternalServerError, getErr.Error())
}
return
}
httpResponse.ResponseWithJson(w, http.StatusOK, book)
fmt.Println(book)
}

func UpdateBook(w http.ResponseWriter, r *http.Request) {
bookID := mux.Vars(r)["isbn"]
id, idErr := strconv.ParseInt(bookID, 10, 64)
if idErr != nil {
httpResponse.ResponseWithError(w, http.StatusBadRequest, "invalid book isbn")
return
}
var book model.Book
decoder := json.NewDecoder(r.Body)
err := decoder.Decode(&book)
if err != nil {
httpResponse.ResponseWithError(w, http.StatusBadRequest, "invalid json data")
return
}
updateErr := book.Update(id)
if updateErr != nil {
switch updateErr {
case sql.ErrNoRows:
httpResponse.ResponseWithError(w, http.StatusNotFound, "book not found")
default:
httpResponse.ResponseWithError(w, http.StatusInternalServerError, updateErr.Error())
}
return
}
httpResponse.ResponseWithJson(w, http.StatusOK, book)
}

func DeleteBook(w http.ResponseWriter, r *http.Request) {
bookID := mux.Vars(r)["isbn"]
id, idErr := strconv.ParseInt(bookID, 10, 64)
if idErr != nil {
httpResponse.ResponseWithError(w, http.StatusBadRequest, "invalid book id")
return
}
var book model.Book
deleteErr := book.Delete(id)
if deleteErr != nil {
switch deleteErr {
case sql.ErrNoRows:
httpResponse.ResponseWithError(w, http.StatusNotFound, "book not found")
default:
httpResponse.ResponseWithError(w, http.StatusInternalServerError, deleteErr.Error())
}
return
}
httpResponse.ResponseWithJson(w, http.StatusOK, map[string]string{"message": "deleted successfully"})
}

func GetAllBook(w http.ResponseWriter, r *http.Request) {
books, getErr := model.GetAllBooks()
fmt.Println("waiii")
if getErr != nil {
httpResponse.ResponseWithError(w, http.StatusBadRequest, getErr.Error())
return
}
httpResponse.ResponseWithJson(w, http.StatusOK, books)
fmt.Println("last ig",books)
}
