package model

import (
	"myapp/datastore/postgres"
)
type Book struct {
	ISBN int64 `json:"isbn"`
	Title string `json:"title"`
	Author string `json:"author"`
	Genre string `json:"genre"`
	
}

const queryInsertUser = "INSERT INTO book (ISBN, Title,Author,Genre) VALUES($1,$2,$3,$4)"
func (s *Book) Create()error{
	// wont return anything but will save 
	_,err :=postgres.Db.Exec(queryInsertUser,s.ISBN,s.Title,s.Author,s.Genre)
	// fmt.Println(fff)
	return err
}

func (s *Book) Read() error{
	const queryGetUser = "SELECT ISBN,Title,Author,Genre FROM book WHERE ISBN =$1;"
	return postgres.Db.QueryRow(queryGetUser,s.ISBN).Scan(&s.ISBN,&s.Title,&s.Author,&s.Genre)
}

func (s *Book) Update(a int64) error{
	const queryUpdateUser = "UPDATE book SET ISBN= $1,Title = $2, Author=$3, Genre= $4 WHERE ISBN= $5 RETURNING ISBN;"
	return postgres.Db.QueryRow(queryUpdateUser,s.ISBN,s.Title,s.Author,s.Genre,a).Scan(&s.ISBN)
}

func (s *Book) Delete(a int64) error {
	const queryDeleteUser = "DELETE  FROM book WHERE ISBN = $1 RETURNING ISBN;"
	return postgres.Db.QueryRow(queryDeleteUser,a).Scan(&s.ISBN)
}

func GetAllBooks()([]Book,error) {
	const queryGetAll = "SELECT * FROM book;"
	table,err := postgres.Db.Query(queryGetAll)
	if err != nil {
		return nil, err
	}
	students := []Book{}
	for table.Next() {
		var s Book 
		dbErr := table.Scan(&s.ISBN,&s.Title,&s.Author,&s.Genre)
		if dbErr != nil {
			return nil, dbErr
		}
		students = append(students,s)

	}
	table.Close()
	return students,nil
}

