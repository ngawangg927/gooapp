CREATE TABLE Book (
    isbn int NOT NULL,
    title VARCHAR(255) NOT NULL,
    author VARCHAR(255) NOT NULL,
    genre VARCHAR(255) NOT NULL,
    PRIMARY KEY (isbn),
    UNIQUE (title)
);