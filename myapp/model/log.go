package model

import "myapp/datastore/postgres"

type Log struct {
	Stdid int `json:"stdid"`
	Title string `json:"title"`
	TakenDate string `json:"dateT"`
	ReturnedDate string `json:"dateR"`
}

func (l *Log) LogData() error {
	const query = "insert into log (Stdid,Title,Taken_Date,Return_date) Values($1,$2,$3,$4);"
	_,err := postgres.Db.Exec(query,l.Stdid,l.Title,l.TakenDate,l.ReturnedDate)
	return err
}

func (l *Log) GetLoggedData(sid int, title string)(error){
	const query = "select * from log where Stdid = $1 and Title = $2;"
	return postgres.Db.QueryRow(query,sid,title).Scan(&l.Stdid,&l.Title,&l.TakenDate,&l.ReturnedDate)
}

func  GetAllEnrolledData() ([]Log,error) {
	const query = "select * from log;"
	table, err := postgres.Db.Query(query)
	if err != nil {
		return nil, err
	}
	enrolls := []Log{}
	for table.Next(){
		var e Log
		dbErr := table.Scan(&e.Stdid,&e.Title,&e.TakenDate,&e.ReturnedDate)
		if dbErr != nil{
			return nil, dbErr
		}
		enrolls = append(enrolls,e)
	}
	table.Close()
	return enrolls,nil
}

func DeleteLoggedData(id int, title string)error{
	const query = "delete from log where Stdid = $1 and Title = $2;"
	_,err := postgres.Db.Exec(query,id,title)
	return err
}
