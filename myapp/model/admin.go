package model

import "myapp/datastore/postgres"


type Admin struct {
	FirstName string `json:"fname"`
	LastName string `json:"lname"`
	Email string `json:"email"`
	Password string `json:"password"`
}

func (a *Admin) Create() error {
	const queryCreateUser = "INSERT INTO admin (firstname,lastname,email,password) VALUES($1,$2,$3,$4)RETURNING email"
	_,err := postgres.Db.Exec(queryCreateUser,a.FirstName,a.LastName,a.Email,a.Password)
	return err
}

func (a *Admin) Check() error {
	const queryCheck = "SELECT email, password FROM admin WHERE email = $1 and password=$2;"
	err := postgres.Db.QueryRow(queryCheck, a.Email, a.Password).Scan(&a.Email,&a.Password)
	return err
}