CREATE TABLE Student (
    stdid int NOT NULL,
    name VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL,
    PRIMARY KEY (stdid),
    UNIQUE (email)
);
CREATE TABLE Booklist (
    isbn int NOT NULL,
    title VARCHAR(255) NOT NULL,
    author VARCHAR(255) NOT NULL,
    genre VARCHAR(255) NOT NULL
    PRIMARY KEY (isbn),
    UNIQUE (title)
);