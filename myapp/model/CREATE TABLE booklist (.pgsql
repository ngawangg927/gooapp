create table enroll (
    std_id int not null,
    Title varchar(45) not null,
    date varchar(45) default null,
    primary key (std_id,Title),
    constraint _fk foreign key (course_id) references book 
    (CourseId) on delete cascade on update cascade,
    CONSTRAINT std_fk foreign key (std_id) references student
    (StdID) on delete cascade on update cascade
)