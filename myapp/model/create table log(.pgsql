create table log(
    StdId int not null,
    Title varchar(45) not null,
    Taken_Date varchar(45) not null,
    Return_date varchar(45) not null,
    primary key (StdId,Title),
    constraint std_fk foreign key (StdId) references Student (stdid) on delete cascade on update cascade,
    constraint title_fk foreign key (Title) references Book (title) on delete cascade on update cascade
)