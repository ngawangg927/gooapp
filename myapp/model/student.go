package model

import (
	"fmt"
	"myapp/datastore/postgres"
)

type Student struct {
StdID           int64  `json:"stdid"`
Name            string `json:"name"`
Email           string `json:"email"`
}

func (s *Student) Create() error {
	fmt.Println("reaching")
const queryInsertUser = "INSERT INTO Student (StdID, Name,  Email) VALUES($1,$2,$3)"
_, err := postgres.Db.Exec(queryInsertUser, s.StdID, s.Name,  s.Email)
return err
}

func (s *Student) Read() error {
const queryGetUser = "SELECT StdID, Name,  Email FROM Student WHERE StdID =$1;"
return postgres.Db.QueryRow(queryGetUser, s.StdID).Scan(&s.StdID, &s.Name, &s.Email)
}

func (s *Student) Update(a int64) error {
const queryUpdateUser = "UPDATE Student SET StdID = $1, Name = $2, Email  = $3 WHERE StdID = $4 RETURNING StdID;"
return postgres.Db.QueryRow(queryUpdateUser, s.StdID, s.Name ,s.Email,a).Scan(&s.StdID)
}

func (s *Student) Delete(a int64) error {
const queryDeleteUser = "DELETE FROM Student WHERE StdID = $1 RETURNING StdID;"
return postgres.Db.QueryRow(queryDeleteUser, a).Scan(&s.StdID)
}

func GetAllStudent() ([]Student, error) {
const queryGetAll = "SELECT * FROM Student;"
table, err := postgres.Db.Query(queryGetAll)
if err != nil {
return nil, err
}
students := []Student{}
for table.Next() {
var s Student
dbErr := table.Scan(&s.StdID, &s.Name, &s.Email)
if dbErr != nil {
return nil, dbErr
}
students = append(students, s)
}
table.Close()
return students, nil
}
