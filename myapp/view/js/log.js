window.onload = function () {
  fetch("/students")
    .then((res) => res.text())
    .then((data) => getStudents(data))
    .catch((e) => {
      alert(e);
    }),
    fetch("/books")
      .then((resp) => resp.text())
      .then((data) => showCourses(data))
      .catch((e) => {
        alert(e);
      }),
    fetch("/showAllEnrolled")
      .then((resp) => resp.text())
      .then((data) => showAllEnrolled(data))
      .catch((e) => {
        alert(e);
      });
};

function showAllEnrolled(data) {
  const enrolls = JSON.parse(data);
  enrolls.forEach((element) => {
    newRow(element);
  });
}
function addLog() {
  console.log("hoooo")
  var currentDate = new Date();
  var formattedDate = currentDate.toLocaleDateString();
  currentDate.setDate(currentDate.getDate() + 7);
  var formattedDateWithWeek = currentDate.toLocaleDateString();
  console.log(formattedDateWithWeek);
  var data = {
    stdid: parseInt(document.getElementById("sid").value),
    title: document.getElementById("ISBN").value,
    dateT: formattedDate,
    dateR: formattedDateWithWeek
  };
  fetch("/enroll", {
    method: "POST",
    body: JSON.stringify(data),
    headers: { "content-type": "application/json; charset=utf-8" },
  })
    .then((res) => {
      if (res.status == 200) {
        console.log("enrooll");
        fetch("/showEnrolled/" + data.stdid + "/" + data.title)
          .then((resp) => resp.text())
          .then((data) => showDatum(data));
      } else {
        throw new Error(res.statusText);
      }
    })
    .catch((e) => {
      alert(e);
    });
}

function showDatum(data) {
  let info = JSON.parse(data);
  newRow(info);
}

function newRow(info) {
  var table = document.getElementById("myTable");
  var row = table.insertRow(table.length);
  var td = [];

  for (i = 0; i < table.rows[0].cells.length; i++) {
    td[i] = row.insertCell(i);
  }
  td[0].innerHTML = info.stdid;
  td[1].innerHTML = info.title;
  td[2].innerHTML = info.dateT;
  td[3].innerHTML = info.dateR;
  td[4].innerHTML = '<input type="button" onclick="deleteEnroll(this)" value="Delete" id="button-1"/>';
  resetForm();
}

function resetForm() {
  document.getElementById("sid").value = "";
  document.getElementById("ISBN").value = "";
}
function deleteEnroll(r) {
  selectedRow = r.parentElement.parentElement;
  stdid = selectedRow.cells[0].innerHTML;
  title = selectedRow.cells[1].innerHTML;
  console.log(typeof id);

  if (
    confirm(
      `Are you sure you want to delete the enrollment of student ${stdid} from the course ${title}`
    )
  ) {
    fetch("/enroll/" + stdid + "/" + title, {
      method: "DELETE",
      headers: { "Content-type": "application/json; charset=UTF-8" },
    })
      .then((response) => {
        if (response.ok) {
          var rowIndex = selectedRow.rowIndex;
          if (rowIndex > 0) {
            selectedRow.parentElement.deleteRow(selectedRow.rowIndex);
          }
          selectedRow = null;
        } else {
          throw new Error(response.statusText);
        }
      })
      .catch((e) => {
        alert(e);
      });
  }
}

function getStudents(data) {
  alert("hi");
  const studentIds = [];
  const students = JSON.parse(data);
  console.log(students);
  students.forEach((stud) => {
    console.log(stud);
    console.log(typeof stud, "kkkkk");
    studentIds.push(stud.stdid);
  });
  console.log(studentIds);
  var select = document.getElementById("sid");
  for (var i = 0; i < studentIds.length; i++) {
    var sidd = studentIds[i];
    console.log(sidd);
    var option = document.createElement("option");
    option.innerHTML = sidd;
    option.value = sidd;
    select.appendChild(option);
  }
}

function showCourses(data) {
  alert("kkkkk");
  const courseIds = [];
  const courses = JSON.parse(data);
  courses.forEach((element) => {
    console.log(element.title);
    console.log(typeof element, "jagn");
    courseIds.push(element.title);
  });
  console.log(courses);
  console.log(courseIds.length, "kkskdfksd");
  var select = document.getElementById("ISBN");
  for (var i = 0; i < courseIds.length; i++) {
    var cidd = courseIds[i];
    console.log("hiiiiiiii");
    var option = document.createElement("option");
    option.innerHTML = cidd;
    option.value = cidd;
    console.log(cidd, "kdkdk");
    select.appendChild(option);
  }
}
