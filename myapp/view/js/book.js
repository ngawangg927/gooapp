window.onload = function () {
  fetch("/books")
    .then((response) => response.text())
    .then((data) => showbooks(data));
};

function showbooks(a) {
  const books = JSON.parse(a);
  books.forEach((stud) => {
    newRow(stud);
  });
}
function AddBook() {
  var data = {
    isbn: parseInt(document.getElementById("ISBN").value),
    title: document.getElementById("Title").value,
    author: document.getElementById("Author").value,
    genre: document.getElementById("Genre").value,
  };
  //second argument is not needed if get method is used
  var isbn = data.isbn;
  console.log(data);
  if (isNaN(isbn)) {
    alert("Enter valid ISBN");
    return;
  } else if (data.title == "") {
    alert("Title cannot be empty");
    return;
  } else if (data.author == "") {
    alert("Author should be Present ");
    return;
  } else if (data.genre == "") {
    alert("Please mention the genre ");
    return;
  }
  fetch("/books", {
    method: "POST",
    body: JSON.stringify(data),
    headers: { "Content-type": "application/json; charset=UTF-8" },
  })
    .then((response) => {
      if (response.ok) {
        fetch("/books/" + isbn)
          .then((response) => response.text())
          .then((data) => showbook(data));
        resetForm();
      } else {
        throw new Error(response.statusText);
      }
    })
    .catch((e) => {
      alert(e);
    });
}

function showbook(a) {
  //convert data to Json
  console.log(a);

  const books = JSON.parse(a);
  newRow(books);
}

function resetForm() {
  document.getElementById("ISBN").value = "";
  document.getElementById("Title").value = "";
  document.getElementById("Author").value = "";
  document.getElementById("Genre").value = "";
}

function newRow(books) {
  //find a table element with id='myTable':
  var table = document.getElementById("myTable");

  var row = table.insertRow(table.length);

  var td = [];
  for (i = 0; i < table.rows[0].cells.length; i++) {
    td[i] = row.insertCell(i);
  }
  td[0].innerHTML = books.isbn;
  td[1].innerHTML = books.title;
  td[2].innerHTML = books.author;
  td[3].innerHTML = books.genre;
  td[4].innerHTML =
    '<input type="button" onclick="deleteBook(this)" value="delete" id="button-1"/>';
  td[5].innerHTML =
    '<input type="button" onclick="updatebook(this)" value="Update" id="button-2"/>';
}

function updatebook(r) {
  selectedRow = r.parentElement.parentElement;
  console.log(selectedRow.cells[0].innerHTML);
  document.getElementById("ISBN").value = selectedRow.cells[0].innerHTML;
  document.getElementById("Title").value = selectedRow.cells[1].innerHTML;
  document.getElementById("Author").value = selectedRow.cells[2].innerHTML;
  document.getElementById("Genre").value = selectedRow.cells[3].innerHTML;

  addButton = document.getElementById("button-add");
  isbn = selectedRow.cells[0].innerHTML;
  addButton.setAttribute("onclick", "update(isbn)");
  addButton.innerHTML = "Update";
}

function getFormData() {
  var data = {
    isbn: parseInt(document.getElementById("ISBN").value),
    title: document.getElementById("Title").value,
    author: document.getElementById("Author").value,
    genre: document.getElementById("Genre").value,
  };
  return data;
}

function update(isbn) {
  var data = getFormData();
  fetch("/books/" + isbn, {
    method: "PUT",
    body: JSON.stringify(data),
    headers: { "Content-type": "application/json; charset=UTF-8" },
  })
    .then((response) => {
      if (response.ok) {
        selectedRow.cells[0].innerHTML = data.isbn;
        selectedRow.cells[1].innerHTML = data.title;
        selectedRow.cells[2].innerHTML = data.author;
        selectedRow.cells[2].innerHTML = data.genre;
        addButton = document.getElementById("button-add");
        addButton.innerHTML = "Add";
        addButton.setAttribute("onclick", "AddStudent()");
        selectedRow = null;
        resetForm();
      } else {
        throw new Error(response.statusText);
      }
    })
    .catch((e) => {
      alert(e);
    });
}

var selectedRow;
function deleteBook(r) {
  selectedRow = r.parentElement.parentElement;
  isbn = selectedRow.cells[0].innerHTML;
  console.log(typeof isbn);
  if (confirm("Are you sure you want to delete this book?")) {
    fetch("/books/" + isbn, {
      method: "DELETE",
      headers: { "Content-type": "application/json; charset=UTF-8" },
    })
      .then((response) => {
        if (response.ok) {
          var rowIndex = selectedRow.rowIndex;
          if (rowIndex > 0) {
            document.getElementById("myTable").deleteRow(rowIndex);
          }
          selectedRow = null;
        } else {
          throw new Error(response.statusText);
        }
      })
      .catch((e) => {
        alert(e);
      });
  }
}
