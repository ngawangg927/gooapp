module myapp

go 1.20

require (
	github.com/gorilla/mux v1.8.0
	github.com/lib/pq v1.10.9
)

require (
	fyne.io/fyne/v2 v2.3.5 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/stretchr/testify v1.8.4 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
