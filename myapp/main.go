package main

import (
	routes "myapp/routes"
)


func main() {
	routes.InitializeRoutes()
}