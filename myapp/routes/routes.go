package routes

import (
	"log"
	"myapp/controller"
	"net/http"

	"github.com/gorilla/mux"
)

func InitializeRoutes() {
	router := mux.NewRouter()

	// Routes for signup
	router.HandleFunc("/signup",controller.AddUserHandler).Methods("POST")
	router.HandleFunc("/login",controller.LoginHandler).Methods("POST")

	// Routes for books
	router.HandleFunc("/books", controller.AddBook).Methods("POST")
	router.HandleFunc("/books/{isbn}", controller.GetBook).Methods("GET")
	router.HandleFunc("/books/{isbn}", controller.UpdateBook).Methods("PUT")
	router.HandleFunc("/books/{isbn}", controller.DeleteBook).Methods("DELETE")
	router.HandleFunc("/books", controller.GetAllBook).Methods("GET")

	// Routes for students
	router.HandleFunc("/students", controller.AddStudent).Methods("POST")
	router.HandleFunc("/students/{stdID}", controller.GetStud).Methods("GET")
	router.HandleFunc("/students/{stdid}", controller.UpdateHandler).Methods("PUT")
	router.HandleFunc("/students/{stdid}", controller.DeleteStudent).Methods("DELETE")
	router.HandleFunc("/students", controller.GetAllStuds).Methods("GET")

	
	// log 
	router.HandleFunc("/enroll",controller.Enroll).Methods("POST")
	router.HandleFunc("/showEnrolled/{stdid}/{title}",controller.ShowEnrolled).Methods("GET")
	router.HandleFunc("/showAllEnrolled",controller.GetAllEnrolled).Methods("GET")
	router.HandleFunc("/enroll/{stdid}/{title}",controller.DeleteLog).Methods("DELETE")
	// router.HandleFunc("/logen",controller.logen).Methods("GET")
	// router.HandleFunc("/enroll/{stdid}/{courseid}",controller.DeleteEnrolled).Methods("DELETE")
	
	// Serve static files
	fhandler := http.FileServer(http.Dir("./view"))
	router.PathPrefix("/").Handler(fhandler)

	err := http.ListenAndServe("localhost:8080", router)
	if err != nil {
		log.Fatal("Error starting the server: ", err)
	}
	log.Println("Application is running on port 8082...")
}
